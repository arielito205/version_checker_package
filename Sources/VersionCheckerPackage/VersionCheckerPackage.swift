
import UIKit

//Model
struct AppInfo: Codable {
    var version:String
}


//
public struct VersionCheckerPackage {
    public private(set) var text = "Hello, World!"
    
    public var baseURL = ""

    public init() {
    }
    
    private func loadJson(fromURLString urlString: String, completion: @escaping (Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                }
                if let data = data {
                    completion(.success(data))
                }
            }
            urlSession.resume()
        }
    }
    
    func checkVersion(result: @escaping (Result<AppInfo, Error>) -> Void) {
//        let url = URL(string: "https://apps.jfmaguire.net/installer/jfmaguire/AppInfo.json?1.0")!
        self.loadJson(fromURLString: "https://apps.jfmaguire.net/installer/jfmaguire/AppInfo.json?1.0") { result in
            switch result {
            case .success(let data):
                let json = try! JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                do {
                    let appInfo = try JSONDecoder().decode(AppInfo.self, from: data)
                    print(appInfo.version)
                }
                catch {
                    print(String(describing: error ))
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
