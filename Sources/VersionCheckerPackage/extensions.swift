//
//  extensions.swift
//  
//
//  Created by Ariel Antonio Fundora López on 19/10/21.
//  Copyright © 2021 Ariel. All rights reserved.

import Foundation
import UIKit

extension Bundle {

    var shortVersion: String {
        if let result = infoDictionary?["CFBundleShortVersionString"] as? String {
            return result
        } else {
            assert(false)
            return ""
        }
    }

    var buildVersion: String {
        if let result = infoDictionary?["CFBundleVersion"] as? String {
            return result
        } else {
            assert(false)
            return ""
        }
    }

    var version: String {
        return "\(shortVersion)(\(buildVersion))"
    }
    
    
    func shouldUpdate(remoteVersion:Double) -> Bool {
        return remoteVersion > Double(Bundle.main.shortVersion)!
    }
}

extension UIViewController {
    
    /**This method shows alertView in the ViewController with title, message and always Accept*/
    func showAlert(_ title: String = "Aviso",
                   message: String,
                   actionAlwaysAccept: UIAlertAction =  UIAlertAction(title: "Aceptar", style: .cancel, handler: nil) ) {
        let alert: UIAlertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(actionAlwaysAccept)
        self.present(alert, animated: true, completion: nil)
    }
    
    /**This method shows alertView in the ViewController with title, message and several actions buttons*/
    func showAlert(_ title: String = "Aviso", message: String, actions: [UIAlertAction]) {
        let alert: UIAlertController = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        for action in actions {
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
}
